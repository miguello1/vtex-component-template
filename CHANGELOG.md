# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### 0.0.1 (2023-12-21)


### Features

* **commitlint:** 🚑️ add commitlint for better commit message validation ([9abaaea](https://gitlab.com/miguello1/vtex-component-template/commit/9abaaea1e020c8a5e210e17529a5ae2fd181b191))
* **package:** Update scripts for releases ([56ba856](https://gitlab.com/miguello1/vtex-component-template/commit/56ba85641f27dc3af65012ccd497c5b2e3970e3b))
* **release script:** ⚡️ add release:patch script to package.json ([b7d2ac8](https://gitlab.com/miguello1/vtex-component-template/commit/b7d2ac8eed3efb9e0833de27191674187b2a0b16))

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
- Initial release.

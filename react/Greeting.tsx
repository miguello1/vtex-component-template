import React from 'react'

type Props = {
  title: string
}

const Greeting: VTEXCustomComponent<Props> = ({ title }: Props) => {
  return <div>Hey, {title}</div>
}

Greeting.defaultProps = {
  title: 'Default value',
}

Greeting.schema = {
  title: 'Custom Component',
  type: 'object',
  properties: {
    name: {
      type: 'string',
      default: 'Default value',
      title: 'Title prop',
    },
  },
}

export default Greeting
